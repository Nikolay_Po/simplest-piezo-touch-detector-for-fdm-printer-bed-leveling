/*
   File:   main.h
   Author: nikolaypo

   Created on June 5, 2021, 1:06 PM
*/

#ifndef MAIN_H
#define	MAIN_H

#ifdef	__cplusplus
extern "C" {
#endif

//Length of sliding average filter, used for fast threshold detection
#define FilterSize 64 //samples per channel, 16..128 recommended

//Size of median filter, 2^n, n value, used for basel level detection
#define MedianOrder 16U

//Trigger thresholds over median channel value to detect a touch
//Negative (sink) sensor current detection
//Possible range is from 1 (highest sensitivity) to FilterSize (no sensitivivy at all, will not trigger)
#define ThresholdNeg0 8 //Channel 0 filtered bias singal negative threshold
#define ThresholdNeg1 8 //Channel 1 filtered bias singal negative threshold
#define ThresholdNeg2 8 //Channel 2 filtered bias singal negative threshold
#define ThresholdNeg3 8 //Channel 3 filtered bias singal negative threshold

//ADC trigger thresholds over bias regulation level
//Positive (source) sensor current detection
#define ThresholdPos0 4 //Channel 0 ADC singal positive threshold
#define ThresholdPos1 4 //Channel 1 ADC singal positive threshold
#define ThresholdPos2 4 //Channel 2 ADC singal positive threshold
#define ThresholdPos3 4 //Channel 3 ADC singal positive threshold

//Hold time
#define HoldTime 50 //Time for touch trigger signal prolongation, milliseconds

#include <stdint.h>
#include <avr/io.h>

typedef struct {
  unsigned Touch: 1; //Touch detected!
  unsigned ADCdata : 1; //ADC cycle completed, 4 channels converted
  unsigned LagDetected : 1; //Main code lag detected (debug purpose)
} Flags_t;

extern volatile Flags_t Flags; //Flags global declaration

//Define fatst trigger output actions, both for open drain and LED
//LED is on Arduino Nano board, D2 is open collector output
//Trigger output is active low (current sinc). Hi-Z when incactive.
#define TriggerOn() do{DDRD|=(1<<DDD2);PORTB|=1<<PORTB5;}while(0)
#define TriggerOff() do{DDRD&=~(1<<DDD2);PORTB&=~(1<<PORTB5);}while(0)

//Just LED control separately
#define LEDon() PORTB|=1<<PORTB5
#define LEDoff() PORTB&=~(1<<PORTB5)

#ifdef	__cplusplus
}
#endif

#endif	/* MAIN_H */
