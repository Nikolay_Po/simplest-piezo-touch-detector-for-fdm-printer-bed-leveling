#ifndef ADC_H
#define ADC_H

#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"

#define BiasThreshold 233  //Should be less than 254

#if FilterSize > 255
  error("Filter size should not exceed 255. 16..64 recommended")
#endif

#if (BiasThreshold+ThresholdPos0)>=254
  error("Decrease ThresholdPos0 value or decrease BiasThreshold value")
#endif

#if (BiasThreshold+ThresholdPos1)>=254
  error("Decrease ThresholdPos1 value or decrease BiasThreshold value")
#endif

#if (BiasThreshold+ThresholdPos2)>=254
  error("Decrease ThresholdPos2 value or decrease BiasThreshold value")
#endif

#if (BiasThreshold+ThresholdPos3)>=254
  error("Decrease ThresholdPos3 value or decrease BiasThreshold value")
#endif

extern volatile uint8_t FilterOut[];  //Channel filter output values
extern volatile uint8_t BiasMonitor; //Debug value

void ADCconfigure(void);
void ADCstart(void);

#ifdef __cplusplus
}
#endif

#endif /* ADC_H */
