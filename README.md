This is the simplest touch sensor for 3D FDM printer. It designed to detect extruder nozzle touch to a bed in a process called "bed leveling".
The detector doesn't require any electronic components except Arduino Nano, 4 piezo elements and the wires. Despite of simplicity, it has 4 independnt piezo element channels and is capable to detect nozzle touch with as low displacement as 0.1mm and as fast as about 2ms from actual touch @10mm/s nozzle descent speed.
Electrical connection diagram is present. 7..12V supply power is recommended to decrease power supply noise. In this case own Arduino Nano regulator is used. But if it is more convenient, supplying of 5V to pin 27 "+5V" instead of supplying +12V to pin 30 is possible.

The project was developed first on bare metal in MPLAB X IDE and is tied to hardware features of ATmega328P microcontroller. That is why it may not work on other Arduinos. It will not compile. Only for convenience of Arduino users I put the souces into Arduino IDE. For luck, the compiler is the same (AVR-GCC) and only minor modification required to build this code in Arduino IDE.

Source code is self-explanatory and provided with comments. In general, the circuit is keeping stable voltage at piezo elements of 1V (0.9-1.1V, depending on IC sample and conditions). ADC input is discharging piezo element by it's 14pF sample and hold capacitor every time the sample is taken. Negative current of piezo element being bended, decreases the voltage at ADC input even more. Every time when voltage decease is detected, to compensate the charge weak pull-up at ADC input is activated for short time. This way weak pull-up current represents piezo element current and is accounted and filtered to make a decision about the touch. The filter is sliding average type, actually sliding sum.

Because ATmega328P ADC structure, there is no need for external components at analog inputs. Input voltage decrease is provided by ADC sampling, input voltage increase is performed by weak pull-up on the analog pin. Both process are under control. The sampling running at fixed 19230sps per channel, weak pull-up is used only when input voltage increase is needed. Both, sampling and pull-up, allows to keep the voltage on the input regulated.

Main parameters of detector are defined in main.h file.

```
#define FilterSize 56 //Lenght of filter, samples per channel
#define TriggerThreshold 16 //Threshold of filtered sensor value to decide the touch occured
#define HoldTime 250 //Time for touch trigger signal prolongation, milliseconds
```

The parameters above are the best for mine certain printer where this device was developed. I examined raw data form sensors and discovered that the lowest filter size where touch pulse become distinguishable is about 16. Below that size the noize peaks may be over the signal. Up to size of 78 (in my certain setup) the signal to noise ratio (peak signal to peak noise) is rising. But the delay of triggering is rising too. At length of 41 I got +10dB SNR. Lenght of 56 providing +2dB more SNR with tolerable increase of delay.
You may test your own values.

**About piezo elements polarty. Important.**
Release 0.22 firmware supports only negative sensor current detection. That is why the polarity of piezo elements connection is important. Take a look at installation example photo. In this setup the elements are bending under a touch center up, edges down. And for this case sensor's base plates are connected to common wire (ground), silver plating of each element is connected to dedicated ADC input.
It is better to place piezo element in such a way to make them bent center to silver plating side. In such case the larger and closer to metal chassis metal plates become connected to detector ground and electrical noise possibility is least.
