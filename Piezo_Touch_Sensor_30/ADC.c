#ifdef __cplusplus
extern "C" {
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include "ADC.h"
#include "main.h"

void ADCconfigure(void) {
  DDRC &= ~((1 << DDC3) | (1 << DDC2) | (1 << DDC1) | (1 << DDC0));                                  //Make pins ADC3:ADC0 an inputs.
  DIDR0 = (1 << ADC0D) | (1 << ADC1D) | (1 << ADC2D) | (1 << ADC3D);                                 //Disable digital input buffers at ADC0..ADC3 pins.
  ADMUX = (1 << REFS1) | (1 << REFS0) | (1 << ADLAR) | (0 << MUX0);                                  //1.1V reference, left justified (only 8 bits used). MUX on ADC0.
  ADCSRB = (0 << ACME) | (0b000 << ADTS0);                                                           //ACME disabled (not used). ADTS free running (autosampling).
  ADCSRA = (0 << ADEN) | (0 << ADSC) | (1 << ADATE) | (1 << ADIF) | (1 << ADIE) | (0b100 << ADPS0);  //Maximum ADC sampling speed. Auto trigger enabled.
  ADCSRA |= 1 << ADEN;                                                                               //Turn on the ADC
}

void ADCstart(void) {
  ADCSRA |= 1 << ADSC;  //Start first conversion of free-running mode
}

volatile uint8_t Idx = 0;             //Oldest memory sample pointer
volatile uint8_t FilterOut[4] = {0};  //Channel filter output values
volatile uint8_t BiasMonitor = 0; //Debug value

ISR(ADC_vect) {  //ADC interrupt routine
  static uint8_t Channel = 0;  //Channel scan index
  static uint8_t Memory0[FilterSize] = {0};
  static uint8_t Memory1[FilterSize] = {0};
  static uint8_t Memory2[FilterSize] = {0};
  static uint8_t Memory3[FilterSize] = {0};
  static uint32_t Median0 = 0; //Median values
  static uint32_t Median1 = 0;
  static uint32_t Median2 = 0;
  static uint32_t Median3 = 0;
  uint8_t Raw; //Current ADC sample raw value
  uint8_t New; //Current bias sample value
  int16_t Delta; //Current sample Delta from median
  PORTB |= 1 << PORTB4; //Debug set D12 high
  switch (Channel) {  //Choose channel for next sample
    case 0: //Channel 0 just sampled
      Raw = ADCH;
      if (Raw < BiasThreshold) {  //Regulate bias on ADC channel 0
        PORTC |= 1 << PORTC0;      //Turn on weak pull-up
        PORTC &= ~(1 << PORTC0);   //Turn off weak pull-up
        New = 1; //If channel bias feedeng occured, count sample as "1"
        BiasMonitor |= (1 << 0);
      } else {
        New = 0; //None bias feededng required, count sample as "0"
        BiasMonitor &= ~(1 << 0);
      }
      //Choose input for after next conversion
      ADMUX = (1 << REFS1) | (1 << REFS0) | (1 << ADLAR) | (2 << MUX0);
      FilterOut[0] += New;             //Add new sample to filter sum
      FilterOut[0] -= Memory0[Idx];  //Remove oldest sample from sum
      Delta = (int16_t)FilterOut[0] - (int16_t)(Median0 >> MedianOrder); //Get sighal delta from median
      if (Delta > ThresholdNeg0) { //Check for filter output threshold reaching
        TriggerOn(); //Threshold is reached. Activate touch detector output (turn on current sink)
        Flags.Touch = 1; //Indicate the triggering for main loop signal prolongation timer
      }
      if (Delta > 0) { //Update median value
        Median0++;
      } else {
        if (Delta < 0) {
          Median0--;
        }
      }
      if (Raw >= (BiasThreshold + ThresholdPos0)) { //Check for positive ADC threshold in case of sensor signal is positive
        TriggerOn(); //Threshold is reached. Activate touch detector output (turn on current sink)
        Flags.Touch = 1; //Indicate the triggering for main loop signal prolongation timer
      }
      Memory0[Idx] = New;  //Replace oldest value in memory by newest one
      break;
    case 1:  //Channel 1 just sampled
      Raw = ADCH;
      if (Raw < BiasThreshold) {  //Regulate bias on ADC channel 1
        PORTC |= 1 << PORTC1;      //Turn on weak pull-up
        PORTC &= ~(1 << PORTC1);   //Turn off weak pull-up
        New = 1; //If channel bias feedeng occured, count sample as "1"
        BiasMonitor |= (1 << 1);
      } else {
        New = 0; //None bias feededng required, count sample as "0"
        BiasMonitor &= ~(1 << 1);
      }
      //Choose input for after next conversion
      ADMUX = (1 << REFS1) | (1 << REFS0) | (1 << ADLAR) | (3 << MUX0);
      FilterOut[1] += New;             //Add new sample to filter sum
      FilterOut[1] -= Memory1[Idx];  //Remove oldest sample from sum
      Delta = (int16_t)FilterOut[1] - (int16_t)(Median1 >> MedianOrder); //Get sighal delta from median
      if (Delta > ThresholdNeg1) { //Check for filter output threshold reaching
        TriggerOn(); //Threshold is reached. Activate touch detector output (turn on current sink)
        Flags.Touch = 1; //Indicate the triggering for main loop signal prolongation timer
      }
      if (Delta > 0) { //Update median value
        Median1++;
      } else {
        if (Delta < 0) {
          Median1--;
        }
      }
      if (Raw >= (BiasThreshold + ThresholdPos1)) { //Check for positive ADC threshold in case of sensor signal is positive
        TriggerOn(); //Threshold is reached. Activate touch detector output (turn on current sink)
        Flags.Touch = 1; //Indicate the triggering for main loop signal prolongation timer
      }
      Memory1[Idx] = New;  //Replace oldest value in memory by newest one
      break;
    case 2:  //Channel 2 just sampled
      Raw = ADCH;
      if (Raw < BiasThreshold) {  //Regulate bias on ADC channel 2
        PORTC |= 1 << PORTC2;      //Turn on weak pull-up
        PORTC &= ~(1 << PORTC2);   //Turn off weak pull-up
        New = 1; //If channel bias feedeng occured, count sample as "1"
        BiasMonitor |= (1 << 2);
      } else {
        New = 0; //None bias feededng required, count sample as "0"
        BiasMonitor &= ~(1 << 2);
      }
      //Choose input for after next conversion
      ADMUX = (1 << REFS1) | (1 << REFS0) | (1 << ADLAR) | (0 << MUX0);
      FilterOut[2] += New;             //Add new sample to filter sum
      FilterOut[2] -= Memory2[Idx];  //Remove oldest sample from sum
      Delta = (int16_t)FilterOut[2] - (int16_t)(Median2 >> MedianOrder); //Get sighal delta from median
      if (Delta > ThresholdNeg2) { //Check for filter output threshold reaching
        TriggerOn(); //Threshold is reached. Activate touch detector output (turn on current sink)
        Flags.Touch = 1; //Indicate the triggering for main loop signal prolongation timer
      }
      if (Delta > 0) { //Update median value
        Median2++;
      } else {
        if (Delta < 0) {
          Median2--;
        }
      }
      if (Raw >= (BiasThreshold + ThresholdPos2)) { //Check for positive ADC threshold in case of sensor signal is positive
        TriggerOn(); //Threshold is reached. Activate touch detector output (turn on current sink)
        Flags.Touch = 1; //Indicate the triggering for main loop signal prolongation timer
      }
      Memory2[Idx] = New;  //Replace oldest value in memory by newest one
      break;
    case 3:  //Channel 3 just sampled
      Raw = ADCH;
      if (Raw < BiasThreshold) {  //Regulate bias on ADC channel 3
        PORTC |= 1 << PORTC3;      //Turn on weak pull-up
        PORTC &= ~(1 << PORTC3);   //Turn off weak pull-up
        New = 1; //If channel bias feedeng occured, count sample as "1"
        BiasMonitor |= (1 << 3);
      } else {
        New = 0; //None bias feededng required, count sample as "0"
        BiasMonitor &= ~(1 << 3);
      }
      //Choose input for after next conversion
      ADMUX = (1 << REFS1) | (1 << REFS0) | (1 << ADLAR) | (1 << MUX0);
      FilterOut[3] += New;             //Add new sample to filter sum
      FilterOut[3] -= Memory3[Idx];  //Remove oldest sample from sum
      Delta = (int16_t)FilterOut[3] - (int16_t)(Median3 >> MedianOrder); //Get sighal delta from median
      if (Delta > ThresholdNeg3) { //Check for filter output threshold reaching
        TriggerOn(); //Threshold is reached. Activate touch detector output (turn on current sink)
        Flags.Touch = 1; //Indicate the triggering for main loop signal prolongation timer
      }
      if (Delta > 0) { //Update median value
        Median3++;
      } else {
        if (Delta < 0) {
          Median3--;
        }
      }
      if (Raw >= (BiasThreshold + ThresholdPos3)) { //Check for positive ADC threshold in case of sensor signal is positive
        TriggerOn(); //Threshold is reached. Activate touch detector output (turn on current sink)
        Flags.Touch = 1; //Indicate the triggering for main loop signal prolongation timer
      }
      Memory3[Idx] = New;  //Replace oldest value in memory by newest one
      if (++Idx >= FilterSize) { //Go forward and make filter memory circular
        Idx = 0; //Reset memory index to start next loop
      }
      if (Flags.ADCdata) { //Check was debug data taken by main loop in time?
        Flags.LagDetected = 1; //If not, set Lag flag for indication and investigation
      }
      Flags.ADCdata = 1; //Indicate all 4 channel filtered samples ready
      break;
    default:
      while (1)
        ;  //Wait for watchdog reset
      break;
  }
  if (++Channel >= 4) {
    Channel = 0;
  }
  PORTB &= ~(1 << PORTB4); //Debug set D12 low
}

#ifdef __cplusplus
}
#endif
